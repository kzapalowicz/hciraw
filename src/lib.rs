//! # hciraw
//!
//! `hciraw` is the Rust interface to the HCI raw sockets. It is useful for
//! anybody who wants to have a direct access to the Bluetooth controller.

use libc::c_int;

const BTPROTO_HCI: c_int = 1;

/// Represents the HCI channel, based on Linux kernel definitions
/// from include/net/bluetooth/hci.h
#[allow(missing_docs)]
#[derive(Copy, Clone)]
pub enum HciChannel {
    Raw,
    User,
    Monitor,
    Control,
    Logging,
}

/// Represents the HCI device id. When set to None it is mapped to
/// [`HCI_DEV_NONE`]. When set to Some(number) it is mapped to that number.
///
/// [`HCI_DEV_NONE`]: https://elixir.bootlin.com/linux/v5.6/source/include/net/bluetooth/hci_sock.h#L42
type HciDevice = Option<u16>;

pub mod addr;
mod internal;
pub mod socket;

pub use addr::{HciSocketAddr, ToHciSocketAddr};
pub use socket::HciSocket;
