# hciraw

The Rust interface to the HCI raw sockets. It is useful for anybody who wants
to have a direct access to the Bluetooth controller.

## Example

```rust
use hciraw::{HciChannel, HciRawSocket, HciSocketAddr};

fn main() -> std::io::Result<()> {
    let addr = hciraw::HciSocketAddr::new(None, HciChannel::Control);
    let socket = hciraw::HciSocket::bind(addr)?;

    let sdata: [u8; 6] = [0x01, 0x00, 0xff, 0x00, 0x00];
    let snum = socket.send(&sdata)?;
    println!("Send {} bytes", snum);

    let mut rbuf = vec![0;128];
    rnum = socket.recv(rbuf.as_mut_slice())?;
    println!("Read {} bytes", rnum);
}
```
