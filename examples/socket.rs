use hciraw::{HciChannel, HciSocket, HciSocketAddr};

fn main() {
    let addr = HciSocketAddr::new(None, HciChannel::Control);
    let socket = HciSocket::bind(addr);

    let socket = match socket {
        Ok(s) => s,
        Err(r) => {
            println!("Error binding: {}", r);
            return;
        }
    };

    let data: [u8; 6] = [0x01, 0x00, 0xff, 0xff, 0x00, 0x00];
    let retval = socket.send(&data);

    match retval {
        Ok(n) => println!("written {} bytes\n", n),
        Err(e) => println!("Error! {}\n", e),
    }

    let mut rbuf = vec![0; 128];
    match socket.recv(rbuf.as_mut_slice()) {
        Ok(n) => println!("read {} bytes\n", n),
        Err(e) => println!("Error! {}\n", e),
    }
}
